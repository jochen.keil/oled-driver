import time
import RPi.GPIO as GPIO
from evdev import uinput, UInput, ecodes as e

FPS = 60

KEY_UP_PIN     = 6
KEY_DOWN_PIN   = 19
KEY_LEFT_PIN   = 5
KEY_RIGHT_PIN  = 26
KEY_PRESS_PIN  = 13

KEY1_PIN       = 21
KEY2_PIN       = 20
KEY3_PIN       = 16

KEY_PINS = \
  [ KEY1_PIN, KEY2_PIN, KEY3_PIN
  , KEY_UP_PIN, KEY_DOWN_PIN, KEY_LEFT_PIN, KEY_RIGHT_PIN
  , KEY_PRESS_PIN
  ]

KEY_CODES = \
  [ e.KEY_0, e.KEY_1, e.KEY_2
  , e.KEY_UP, e.KEY_DOWN, e.KEY_LEFT, e.KEY_RIGHT
  , e.KEY_ENTER
  ]

KEY_STATE = set()

try:
  ui = UInput({e.EV_KEY: KEY_CODES}, name='Waveshare', bustype=e.BUS_USB)

  GPIO.setmode(GPIO.BCM)

  for key_pin in KEY_PINS:
    GPIO.setup(key_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(key_pin, GPIO.BOTH)

  while True:
    t = time.time()
    for key_pin, key_code in zip(KEY_PINS, KEY_CODES):
      if GPIO.event_detected(key_pin):
        # key press
        if key_pin not in KEY_STATE:
          print(f'({t}) Pressed: {key_pin}')
          KEY_STATE.add(key_pin)
          ui.write(e.EV_KEY, key_code, 1)
          ui.syn()

        # key release
        elif key_pin in KEY_STATE:
          print(f'({t}) Released: {key_pin}')
          KEY_STATE.discard(key_pin)
          ui.write(e.EV_KEY, key_code, 0)
          ui.syn()

    time.sleep(1.0 / FPS)

  GPIO.cleanup()

except Exception as e:
  GPIO.cleanup()
  print(e)

except:
  GPIO.cleanup()
